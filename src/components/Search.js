import axios from 'axios';
import React, { useState } from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import Location from './Location';

const Search = () => {  
  const [location, setLocation] = useState("");
  const [weather, setWeather]   = useState(undefined);

  const getWeather = (lat, lon) => {
    let options = {
      method: 'GET',
      url: '<SECURE-URL>',
      params: {lon: lon, lat: lat},
      headers: {        
        'x-api-key': '<PUBLIC-API-KEY>'
      }
    };
    
    axios.request(options).then(function (response) {      
      setWeather(response.data.data[0]);
    }).catch(function (error) {
      console.error(error);
    });
  }

  const fwdGeoLocation = (loc) => {  
    let location = [];
    let options = {
      method: 'GET',
      url: '<SECURE-URL>',
      params: {q: loc, 'accept-language': 'en', polygon_threshold: '0.0'},
      headers: {      
        'x-api-key': '<PUBLIC-API-KEY>'
      }
    };  
    axios.request(options).then(function (response) {      
      location = [response.data[0].lat, response.data[0].lon];      
      getWeather(location[0], location[1])
    }).catch(function (error) {
      console.error(error);
    });
  }

  return (        
    <>

    <Form className="search_form">
      <Row className="justify-content-md-center">
        <Col md={{ offset: 3 }}>
          <Form.Control placeholder="Address, city or country name." onChange={event => setLocation(event.target.value)}/>
        </Col>
        <Col>
          <button type="button" className="btn btn-primary" onClick={() => fwdGeoLocation(location)}>
            Search
          </button>
        </Col>
      </Row>
    </Form>   

    <Row className="justify-content-md-center">
      <Col md="auto">
        { typeof weather === 'undefined' ?
          <h4 className="empty_state">Search the weather at any location.</h4>
          :
          <Location weather={weather} />
        }
      </Col>
    </Row>

    </>
  );
}

export default Search;
