import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import Header from './components/Header';
import Search from './components/Search';

function App() {  
  return (
    <div className="App">      
        <Container>
          <Header />    
          <Search />
        </Container>
    </div>
  );
}

export default App;
